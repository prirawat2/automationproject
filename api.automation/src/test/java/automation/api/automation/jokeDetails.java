package automation.api.automation;

public class jokeDetails {

	 private String category;
	 private String type;
	 private String joke;
	 private int formatVersion;
	 private flagDetails flags;
	public String getCategory() {
		return category;
	}
	public String getType() {
		return type;
	}
	public String getJoke() {
		return joke;
	}
	public int getFormatVersion() {
		return formatVersion;
	}
	public flagDetails getFlags() {
		return flags;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setJoke(String joke) {
		this.joke = joke;
	}
	public void setFormatVersion(int formatVersion) {
		this.formatVersion = formatVersion;
	}
	public void setFlags(flagDetails flags) {
		this.flags = flags;
	}
	
}
