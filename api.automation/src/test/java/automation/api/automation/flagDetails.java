package automation.api.automation;

public class flagDetails {

	private boolean nsfw;
	private boolean religious;
	private boolean political;
	private boolean racist;
	private boolean sexist;
	public void setNsfw(boolean nsfw) {
		this.nsfw = nsfw;
	}
	public void setReligious(boolean religious) {
		this.religious = religious;
	}
	public void setPolitical(boolean political) {
		this.political = political;
	}
	public void setRacist(boolean racist) {
		this.racist = racist;
	}
	public void setSexist(boolean sexist) {
		this.sexist = sexist;
	}
	public boolean isNsfw() {
		return nsfw;
	}
	public boolean isReligious() {
		return religious;
	}
	public boolean isPolitical() {
		return political;
	}
	public boolean isRacist() {
		return racist;
	}
	public boolean isSexist() {
		return sexist;
	}
}
