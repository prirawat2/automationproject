package automation.api.automation;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class getData {

	@Test
	public void getMethodFuntionality() {
		RestAssured.baseURI = "https://sv443.net/jokeapi/v2";
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/info");
		
		//Get the status code
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200, "The status code is not correct");
		
		//Get the version number
		JsonPath jsonEvaluator = response.jsonPath();
		String version = jsonEvaluator.get("version");
		System.out.println("Version received is : " + version); 
		Assert.assertEquals(version, "2.1.3", "The version doesn't match");
		
		//Get the Total number of Jokes
		int numberOfJokes = jsonEvaluator.get("jokes.totalCount");
		System.out.println("All Jokes : "+numberOfJokes);
		Assert.assertEquals(numberOfJokes, 192, "Total number of jokes doesn't match");
		
		//Get the List of all Categories
		List<String> allCategories = jsonEvaluator.getList("jokes.categories");
		for(String categories : allCategories) {
			System.out.println("Category : "+ categories);
		}
		
		//Get the response body
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is : " + responseBody); 
	}
}
